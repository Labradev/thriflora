import { FormsModule } from '@angular/forms';
import { screen, render, waitFor, fireEvent } from '@testing-library/angular';
import '@testing-library/jest-dom';
import { FutureCustomerFormComponent } from './future-customer-form.component';
import userEvent from '@testing-library/user-event';

describe('FutureCustomerFormComponent', () => {
  
  const placeholderEmailTextField = 'Enter a valid e-mail address';
  const customerEmail = 'hello@email.com';
  const incorrectlyFormattedCustomerEmail = 'uselessemail';
  const warningEmailIncorrect = 'Email address is incorrect';

  it('gets the placeholder from email field', async () => {
    await renderComponent();

    const placeholderElement = screen.queryByPlaceholderText(placeholderEmailTextField);

    await waitFor(() => expect(placeholderElement).toBeInTheDocument());
  });

  it('is writeable', async () => {
    await renderComponent();
    
    writeDraft(customerEmail);

    await waitFor(() => expect(screen.getByDisplayValue(customerEmail)).toBeInTheDocument());
  });

  it('validates email adress', async () => {
    await renderComponent();
    
    writeDraft(incorrectlyFormattedCustomerEmail);

    await waitFor(() => expect(screen.getByText(warningEmailIncorrect)).toBeInTheDocument());
  });

  it('is a contry selector available', async () => {
    await renderComponent();
    

    expect(screen.getByRole('combobox', { hidden: true })).toBeInTheDocument();
  });

  it('selects a country', async () => {
    await renderComponent();

    await userEvent.selectOptions(screen.getByRole('combobox'), ['italy'])

    expect(screen.getByRole('combobox', { hidden: true })).toHaveValue('italy');
  });

  it('offers to send an email when the store is available', async () => {
    await renderComponent();
    const storeAvailableEmailcheckbox = screen.getByRole('checkbox', {hidden: true});

    fireEvent.click(storeAvailableEmailcheckbox);

    expect(storeAvailableEmailcheckbox).toBeChecked();
  });
  
  const renderComponent = async () => {
    await render(FutureCustomerFormComponent, {
      imports: [FormsModule],
    });
  };

  const writeDraft = async (text: string) => {
    await userEvent.type(screen.getByRole('textbox', { hidden: true }), text);
  };
});
